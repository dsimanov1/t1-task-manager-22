package ru.t1.simanov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Remove all tasks.";

    public static final String NAME = "task-clear";

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
