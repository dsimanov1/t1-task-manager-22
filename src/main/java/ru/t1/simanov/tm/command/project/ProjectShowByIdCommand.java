package ru.t1.simanov.tm.command.project;

import ru.t1.simanov.tm.model.Project;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Show project by id.";

    public static final String NAME = "project-show-by-id";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
