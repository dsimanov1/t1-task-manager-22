package ru.t1.simanov.tm.command.user;

import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "View profile of current user.";

    public static final String NAME = "user-view-profile";

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER PROFILE]");
        showUser(user);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
