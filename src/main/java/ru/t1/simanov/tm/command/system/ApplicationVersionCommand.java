package ru.t1.simanov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";

    public static final String DESCRIPTION = "Show application version.";

    public static final String NAME = "version";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.22.0");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
