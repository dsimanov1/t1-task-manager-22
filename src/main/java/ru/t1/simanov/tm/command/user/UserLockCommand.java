package ru.t1.simanov.tm.command.user;

import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return "user lock";
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
