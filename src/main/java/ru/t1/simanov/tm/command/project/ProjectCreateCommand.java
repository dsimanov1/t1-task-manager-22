package ru.t1.simanov.tm.command.project;

import ru.t1.simanov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Create new project.";

    public static final String NAME = "project-create";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
