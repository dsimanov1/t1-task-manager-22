package ru.t1.simanov.tm.command.task;

import ru.t1.simanov.tm.model.Task;
import ru.t1.simanov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Show tasks by project id.";

    public static final String NAME = "task-show-by-project-id";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY TASK ID]");
        System.out.println("ENTER TASK ID:");
        final String projectId = TerminalUtil.nextLine();
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
