package ru.t1.simanov.tm.command.user;

import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return "user unlock";
    }

    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
