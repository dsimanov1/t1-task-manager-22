package ru.t1.simanov.tm.api.service;

import ru.t1.simanov.tm.api.repository.IUserOwnedRepository;
import ru.t1.simanov.tm.enumerated.Sort;
import ru.t1.simanov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    List<M> findAll(String userId, Sort sort);

}
